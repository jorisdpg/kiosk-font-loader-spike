//
//  FontLoader.swift
//  FontLoader
//
//  Created by Joris van Gasteren on 06/10/2020.
//

import Foundation
import UIKit

final class FontLoader {
    let session: URLSession = .shared
    
    enum FontError: Error {
        case noData
        case createDataProvider
        case createFont
        case postScriptName
        case unknown
    }
    
    func load(url: URL, completion: @escaping (Result<String, Error>) -> Void) {
        let task = session.dataTask(with: url) { (data, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion(.failure(error))
                } else if let data = data {
                    self.load(data: data, completion: completion)
                } else {
                    completion(.failure(FontError.noData))
                }
            }
        }
        
        task.resume()
    }
    
    func load(data: Data, completion: @escaping (Result<String, Error>) -> Void) {
        guard let provider = CGDataProvider(data: data as CFData) else {
            completion(.failure(FontError.createDataProvider))
            return
        }
        
        guard let font = CGFont(provider) else {
            completion(.failure(FontError.createFont))
            return
        }

        guard let fontName = font.postScriptName else {
            completion(.failure(FontError.postScriptName))
            return
        }

        var error: Unmanaged<CFError>?
        
        if CTFontManagerRegisterGraphicsFont(font, &error) {
            completion(.success(fontName as String))
        } else {
            if let error = error?.takeUnretainedValue() {
                completion(.failure(error))
            } else {
                completion(.failure(FontError.unknown))
            }
        }
    }
}
