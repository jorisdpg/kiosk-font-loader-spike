//
//  ViewController.swift
//  FontLoader
//
//  Created by Joris van Gasteren on 06/10/2020.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var label: UILabel!

    let fontLoader = FontLoader()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let url = URL(string: "https://www.jorisvangasteren.com/files/PermanentMarker-Regular.ttf")!
        fontLoader.load(url: url) { result in
            switch result {
            case .success(let fontName):
                let font = UIFont(name: fontName, size: 30)
                self.label.font = font
            case .failure(let error):
                print("Error: \(error)")
            }
        }
    }
}
